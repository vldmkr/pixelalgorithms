﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelAlgorithms
{
    class PixelAlgo
    {
        private PixelPlate _plate;

        private void ResetData()
        {
            RestFillAlgo();
        }

        public PixelPlate Plate
        {
            get
            {
                return _plate;
            }
            set
            {
                _plate = value;
                ResetData();
            }
        }

        public PixelAlgo()
        {
            InitFillAlgo();
        }

        #region FILL

        private Point _begin;
        private Edge _edge;
        private List<Edge> _edges;

        public PixelColor EdgeColor { get; set; }

        public PixelColor FillColor { get; set; }

        private void InitFillAlgo()
        {
            _edges = new List<Edge>();
            EdgeColor = Color.Black.ToPixelColor();
            FillColor = Color.Green.ToPixelColor();
        }

        private void RestFillAlgo()
        {
            _edges.Clear();
            _begin = new Point();
        }

        public void AddPoint(Point p)
        {
            if (Plate != null)
            {
                if (_begin.IsEmpty)
                {
                    _edges.Clear();

                    _begin = p;
                    _edge = new Edge();
                    _edge.Begin = _begin;

                    Plate.MoveTo(_begin);
                    Plate.Plot(_begin, color: EdgeColor);
                }
                else
                {
                    _edge.End = p;
                    _edges.Add(_edge);
                    _edge = new Edge(_edge);
                    _edge.Begin = p;

                    Plate.LineTo(p, color: EdgeColor);
                    Plate.MoveTo(p);
                }
            }
        }
        public void AddPoint(int x, int y)
        {
            AddPoint(new Point(x, y));
        }

        public void CloseFigure()
        {
            if (_edges.Count > 1)
            {
                _edge.End = _begin;
                _edges.Add(_edge);

                Plate.LineTo(_begin);

                _begin = new Point();

                Fill();
            }
        }

        public void Fill()
        {
            var minY = _edges[0].Begin.Y;
            var maxY = _edges[_edges.Count - 1].End.Y;
            for (var i = 0; i < _edges.Count; i++)
            {
                if (_edges[i].Begin.Y > _edges[i].End.Y)
                {
                    var buf = _edges[i].Begin;
                    _edges[i].Begin = _edges[i].End;
                    _edges[i].End = buf;
                }

                if (_edges[i].Begin.Y > maxY)
                {
                    maxY = _edges[i].Begin.Y;
                }
                if (_edges[i].End.Y > maxY)
                {
                    maxY = _edges[i].End.Y;
                }
                if (_edges[i].Begin.Y < minY)
                {
                    minY = _edges[i].Begin.Y;
                }
                if (_edges[i].End.Y < minY)
                {
                    minY = _edges[i].End.Y;
                }
            }
            _edges.Sort(new Comparison<Edge>((lhs, rhs) => { return lhs.Begin.Y - rhs.Begin.Y; }));

            var activeEdges = GetActiveEdges(0, minY);
            var index = activeEdges.Index;
            var Edges = activeEdges.Edges;
            for (var y = minY; y < maxY; y++)
            {

                for (; index < _edges.Count; ++index)
                {
                    if (_edges[index].Begin.Y == y)
                    {
                        Edges.Add(_edges[index]);
                    }
                    else
                    {
                        break;
                    }
                }

                for (var i = 0; i < Edges.Count; i++)
                {
                    if (Edges[i].End.Y == y)
                    {
                        Edges.RemoveAt(i);
                        i--;
                    }
                }

                var crossPoints = GetCrossPoints(y, Edges);
                for (var i = crossPoints.Count-1; i >= 1; i-=2)
                {
                    var b = crossPoints[i];
                    var e = crossPoints[i - 1];
                    Plate.MoveTo(b, y);
                    Plate.HorisontalLineTo(e, FillColor);
                }  
            }
            _edges.Clear();
        }

        private ActiveEdge GetActiveEdges(int index, int y)
        {
            var ret = new ActiveEdge();
            for (var i = index; i < _edges.Count; i++)
            {
                if (_edges[i].Begin.Y == y)
                {
                    ret.Edges.Add(_edges[i]);
                    ret.Index = i;
                }
                else
                {
                    break;
                }
            }
            ret.Index++;
            return ret;
        }

        private List<int> GetCrossPoints(int y, List<Edge> activeEdges)
        {
            var crossPoints = new List<int>();
            foreach (var val in activeEdges)
            {
                var m1 = y - val.Begin.Y;
                var m2 = val.End.X - val.Begin.X;
                var d = val.End.Y - val.Begin.Y;
                var x = (int)Math.Floor((double)(m1 * m2) / d + val.Begin.X);
                crossPoints.Add(x);
            }
            crossPoints.Sort(new Comparison<int>((lhs, rhs) => { return rhs - lhs; }));
            return crossPoints;
        }

        private class Edge
        {
            public Point Begin;
            public Point End;

            public Edge()
            { 
            }
            public Edge(Edge edge)
            {
                Begin = edge.Begin;
                End = edge.End;
            }
        }

        private class ActiveEdge
        {
            public int Index;
            public List<Edge> Edges = new List<Edge>();
        }
        #endregion

    }
}
