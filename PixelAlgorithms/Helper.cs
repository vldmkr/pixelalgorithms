﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelAlgorithms
{
    static class Helper
    {
        public static PixelColor ToPixelColor(this Color color)
        {
            PixelColor pixelColor = new PixelColor(color.R, color.G, color.B, color.A);
            return pixelColor;
        }

        public static Color ToColor(this PixelColor pixelColor)
        {
            return Color.FromArgb(pixelColor.Alpha, pixelColor.Red, pixelColor.Green, pixelColor.Blue);
        }

        public static Color GetRandomColor()
        {
            Random rand = new Random();
            var r = rand.Next(255);
            var g = rand.Next(255);
            var b = rand.Next(255);
            return Color.FromArgb(255, r, g, b);
        }
        public static PointF Center(this RectangleF rect)
        {
            return new PointF(rect.X + rect.Width / 2.0f, rect.Y + rect.Height / 2.0f);
        }

        public static Point GetPoint(this PointF p)
        {
            return new Point((int)p.X, (int)p.Y);
        }
    }
}
