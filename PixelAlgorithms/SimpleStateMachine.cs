﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace PixelAlgorithms
{
    class SimpleStateMachine
    {
        public static readonly int UNDEFINED_STATE = -1;
        public delegate void ActionDelegate (Point? p);

        private List<ActionDelegate> _actionList;

        private int _curState;

        public int CurrentState
        {
            get
            {
                return _curState;
            }
            set
            {
                if (_curState < _actionList.Count)
                {
                    _curState = value;
                }
            }
        }

        public int Count
        {
            get
            {
                return _actionList.Count;
            }
        }

        public SimpleStateMachine()
        {
            _actionList = new List<ActionDelegate>();
            _curState = UNDEFINED_STATE;
        }

        public void PushAction(ActionDelegate action)
        {
            _actionList.Add(action);
        }

        public void PopAction()
        {
            _actionList.RemoveAt(_actionList.Count - 1);
        }

        private void Resolve(int idx, Point? p)
        {
            if (idx > UNDEFINED_STATE && idx < _actionList.Count)
            {
                ActionDelegate action = _actionList[idx];

                if (action != null)
                {
                    action(p);
                }
            }
        }

        public void ResolveCurrentState(Point? p)
        {
            Resolve(CurrentState, p);
        }

    }
}
