﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelAlgorithms
{
    class PixelDrawAdapter
    {
        private Size _realSize;
        private SolidBrush _brushBrush;
        private Pen _penPixelDevider;
        private GraphicsPath _path;
        private Point _moveRealPoint;

        public bool HasGrid { get; set; }
        public Bitmap Context { get; set; }
        public PixelPlate Plate { get; set; }
        public Color ColorBackground { get; set; }
        public Color ColorGrid
        {
            get { return _penPixelDevider.Color; }
            set { _penPixelDevider.Color = value; }
        }
        public byte PixelSize { get; set; }
        public Size PlateSize { get { return Plate == null ? new Size() : new Size(Plate.Width, Plate.Height); } }
        public Size RealSize { get { return _realSize; } }

        public PixelDrawAdapter()
        {
            ColorBackground = Color.White;
            _brushBrush = new SolidBrush(Color.Red);
            _penPixelDevider = new Pen(Color.LightGray);
            _path = new GraphicsPath();
            _moveRealPoint = new Point(0, 0);
        }

        private Size CalcSize(byte pixelSize)
        {
            if (null == Context)
            {
                throw new Exception("Context is null");
            }
            if (0 == pixelSize)
            {
                throw new Exception("PixelSize is not set");
            }
            return new Size((int)Math.Floor(Context.Width / (double)pixelSize), (int)Math.Floor(Context.Height / (double)pixelSize));
        }

        public RectangleF GetPixelRect(Point p)
        {
            if (HasGrid)
            {
                return GetPixelGridRect(p.X, p.Y, 0.8f);
            }
            return GetPixelRealRect(p.X, p.Y);
        }

        public RectangleF GetPixelRect(int x, int y)
        {
            if(HasGrid)
            {
                return GetPixelGridRect(x, y, 0.8f);
            }
            return GetPixelRealRect(x, y);
        }

        public RectangleF GetPixelRealRect(int x, int y)
        {
            return new RectangleF(x * PixelSize, y * PixelSize, PixelSize, PixelSize);
        }

        public Point GetRealPixelCenter(Point pixelPoint)
        {
            return GetPixelRect(pixelPoint).Center().GetPoint();
        }

        public RectangleF GetPixelGridRect(int x, int y, float koefSize)
        {
            float size = PixelSize * koefSize;
            float step = (PixelSize - size) / 2.0f;
            return new RectangleF(x * PixelSize + step, y * PixelSize + step, size, size);
        }

        private void DrawGrid(Graphics g, int width, int height, byte pixelSize)
        {
            var state = g.Save();

            for (var i = 0; i < height; i++)
            {
                g.DrawLine(_penPixelDevider, 0, (i + 1) * pixelSize, pixelSize * width, (i + 1) * pixelSize);
            }
            for (var i = 0; i < width; i++)
            {
                g.DrawLine(_penPixelDevider, (i + 1) * pixelSize, 0, (i + 1) * pixelSize, pixelSize * height);
            }

            g.Restore(state);
        }

        public Point? ToPlateCoords(Point contextPoint)
        {
            int x = contextPoint.X / PixelSize;
            int y = contextPoint.Y / PixelSize;
            if (Plate.IsValidCoords(x, y)) { return new Point(x, y); }
            else return null;
        }
        public void ApplyChanges()
        {
            _realSize = CalcSize(PixelSize);
        }
        public void Draw()
        {
            if (Context != null && Plate != null)
            {
                using (Graphics g = Graphics.FromImage(Context))
                {
                    var state = g.Save();

                    g.Clear(ColorBackground);

                    var width = Math.Min(RealSize.Width, PlateSize.Width);
                    var height = Math.Min(RealSize.Height, PlateSize.Height);

                    if (HasGrid)
                    {
                        DrawGrid(g, width, height, PixelSize);
                    }

                    for (var i = 0; i < width; i++)
                    {
                        for (var j = 0; j < height; j++)
                        {
                            if (Plate[i, j].IsFull)
                            {
                                _brushBrush.Color = Plate[i, j].MainColor.ToColor();
                                g.FillRectangle(_brushBrush, GetPixelRect(i, j));
                            }
                        }
                    }

                    g.Restore(state);
                }
            }
        }
    }
}
