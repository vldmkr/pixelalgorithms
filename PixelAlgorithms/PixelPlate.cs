﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PixelAlgorithms
{
    class PixelPlate
    {
        private List<List<Pixel>> _plate;
        private int _beginX, _beginY;
        private int _width;
        private int _height;

        public int Width { get { return _width; } }
        public int Height { get { return _height; } }
        public int BeginX { get { return _beginX; } }
        public int BeginY { get { return _beginY; } }

        public PixelPlate(int width, int height)
        {
            _beginX = 0;
            _beginY = 0;

            _width = width;
            _height = height;

            _plate = new List<List<Pixel>>(_height);
            for (var i = 0; i < _plate.Capacity; i++)
            {
                _plate.Add(new List<Pixel>(_width));
            }
            for (var i = 0; i < _plate.Capacity; i++)
            {
                for (var j = 0; j < _plate[i].Capacity; j++)
                {
                    _plate[i].Add(new Pixel());
                }
            }
        }

        public bool IsValidCoords(int x, int y)
        {
            bool outValue = !(x >= Width || x < 0 || y >= Height || y < 0);

            Debug.WriteLineIf(!outValue, String.Format("Value 'x' (={0}) must be less than property 'Width '(={1}) and greater than 0. Value 'y'(={2}) must be less than property 'Height'(={3}) and greater than 0", x, Width, y, Height));
            
            return outValue;
        }

        public bool Plot(System.Drawing.Point? point, int alpha = -1, PixelColor? color = null)
        {
            return point.HasValue ? this.Plot(point.Value.X, point.Value.Y, alpha, color) : false;
        }

        public bool Plot(int x, int y, int alpha = -1, PixelColor? color = null)
        {
            if (IsValidCoords(x, y) && !this[x, y].IsFull)
            {
                if (alpha >= 0 && alpha < 255)
                {
                    this[x, y].MainColor = this[x, y].MainColor.ChangeAlpha((byte)alpha);
                }
                if (color.HasValue)
                {
                    this[x, y].MainColor = color.Value;
                }
                this[x, y].IsFull = true;
                return true;
            }
            return false;
        }

        public bool Clean(System.Drawing.Point? point)
        {
            return point.HasValue ? this.Clean(point.Value.X, point.Value.Y) : false;
        }

        public bool Clean(int x, int y)
        {
            if (IsValidCoords(x, y) && this[x, y].IsFull)
            {
                this[x, y].IsFull = false;
                return true;
            }
            return false;
        }

        public void MoveTo(System.Drawing.Point p)
        {
            MoveTo(p.X, p.Y);
        }

        public void MoveTo(int x, int y)
        {
            _beginX = x;
            _beginY = y;
        }

        public void LineTo(int endX, int endY, PixelColor? color = null)
        {
            //http://members.chello.at/~easyfilter/bresenham.html

            int dx = Math.Abs(endX - _beginX);
            int dy = -Math.Abs(endY - _beginY);
            int sx = endX > _beginX ? 1 : -1;
            int sy = endY > _beginY ? 1 : -1;

            int err = dx + dy;
            int err2;

            int x = _beginX;
            int y = _beginY;
            while(true)
            {
                Plot(x, y, color: color);
                if (x == endX && y == endY) break;
                err2 = err << 1;
                if (err2 >= dy) { err += dy; x += sx; }
                if (err2 <= dx) { err += dx; y += sy; }
            }
        }

        public void LineTo(System.Drawing.Point p , PixelColor? color = null)
        {
            LineTo(p.X, p.Y, color);
        }

        public void HorisontalLineTo(int endX, PixelColor? color = null)
        {
            int direction = _beginX > endX ? 1 : -1;
            while (endX != _beginX)
            {
                Plot(endX, _beginY, color: color);
                endX += direction;
            }
        }

        public void AddCircle(int radius, PixelColor? color = null)
        {
            //http://members.chello.at/~easyfilter/bresenham.html

            int x = -radius, y = 0, err = 2 - 2 * radius; //    II. Quadrant
            do
            {
                Plot(_beginX - x, _beginY + y, color: color); //  I. Quadrant
                Plot(_beginX - y, _beginY - x, color: color); //  II. Quadrant
                Plot(_beginX + x, _beginY - y, color: color); //  III. Quadrant
                Plot(_beginX + y, _beginY + x, color: color); //  IV. Quadrant
                radius = err;
                if (radius <= y) { err += ++y * 2 + 1; }
                if (radius > x || err > y) { err += ++x * 2 + 1; }
            } while (x < 0);
        }

        public void SlowFill(int x, int y, PixelColor fill_color)
        {
            if (IsValidCoords(x,y) && !this[x,y].IsFull)
            {
                Plot(x, y, color: fill_color);

                SlowFill(x + 1, y, fill_color);
                SlowFill(x - 1, y, fill_color);
                SlowFill(x, y + 1, fill_color);
                SlowFill(x, y - 1, fill_color);
            } 
        }

        public Pixel this[int x, int y]
        {
            get { return ((Pixel)(_plate[y][x])); }
        }
    }
}
