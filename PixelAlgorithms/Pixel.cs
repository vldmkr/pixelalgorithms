﻿using System;

namespace PixelAlgorithms
{
    class Pixel
    {
        public bool IsFull { get; set; }
        public PixelColor MainColor { get; set; }

        public Pixel()
            : this(false)
        {
        }
        public Pixel(bool full)
            : this(full, new PixelColor(alpha: 255))
        {
        }

        public Pixel(bool full, PixelColor color)
        {
            IsFull = full;
            MainColor = color;
        }
    }
}
