﻿using System;

namespace PixelAlgorithms
{
    struct PixelColor
    {
        byte _red, _green, _blue, _alpha;

        public byte Red { get { return _red; } }
        public byte Green { get { return _green; } }
        public byte Blue { get { return _blue; } }
        public byte Alpha { get { return _alpha; } }

        public PixelColor(byte red = 0, byte green = 0, byte blue = 0, byte alpha = 255)
            : this()
        {
            _red = red;
            _green = green;
            _blue = blue;
            _alpha = alpha;
        }

        public PixelColor ChangeAlpha(byte a)
        {
            return new PixelColor(Red, Green, Blue, a);
        }

        public override bool Equals(Object obj)
        {
            PixelColor p = (PixelColor)obj;
            return this.GetHashCode() == p.GetHashCode();
            //return (_red == p._red) && (_green == p._green) && (_blue == p._blue) && (_alpha == p._alpha);
        }

        public override int GetHashCode()
        {
            return _red.GetHashCode() ^ _green.GetHashCode() ^ _blue.GetHashCode() ^ _alpha.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format("{0:X}{1:X}{2:X}{3:X}", _alpha, _red, _green, _blue);
        }
        public static bool operator ==(PixelColor a, PixelColor b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            return a.Equals(b);
        }

        public static bool operator !=(PixelColor a, PixelColor b)
        {
            return !(a == b);
        }
    }
}
