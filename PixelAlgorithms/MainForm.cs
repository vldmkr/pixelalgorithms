﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PixelAlgorithms
{
    public partial class MainForm : Form
    {
        private PixelRenderer _renderer;

        protected override CreateParams CreateParams
        {
            //How to fix the flickering in User controls
            //http://stackoverflow.com/questions/2612487/how-to-fix-the-flickering-in-user-controls
            get
            {
                var cp = base.CreateParams;
                cp.Style &= ~0x02000000;  // Turn off WS_CLIPCHILDREN
                return cp;
            }
        }

        public MainForm()
        {
            InitializeComponent();
            InitializeMyComponent();
        }

        private void InitializeMyComponent()
        {
            _renderer = new PixelRenderer(70, 40, 10);
            //_renderer.HasGrid = false;

            this.SuspendLayout();
            _renderer.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _renderer.Location = new System.Drawing.Point(0, 0);
            _renderer.Size = this.ClientSize;
            this.Controls.Add(_renderer);
            this.ResumeLayout(false);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _renderer.ResizeContext();
            _renderer.Draw();
        }
        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            _renderer.ResizeContext();
            _renderer.Draw();
        }
    }
}
