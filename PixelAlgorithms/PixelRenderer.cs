﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PixelAlgorithms
{
    class PixelRenderer : Control
    {
        private PixelDrawAdapter _drawAdapter;
        private PixelPlate _plate;
        private PixelAlgo _algo;
        private SimpleStateMachine _stateMachineFirst;
        private SimpleStateMachine _stateMachineSecond;
        private bool _allowedMouseMoveAction;

        protected override CreateParams CreateParams
        {
            //How to fix the flickering in User controls
            //http://stackoverflow.com/questions/2612487/how-to-fix-the-flickering-in-user-controls
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
        public override void Refresh()
        {
            base.Refresh();
            this.Draw();
        }

        public bool HasGrid
        {
            get { return null == _drawAdapter ? false : _drawAdapter.HasGrid; }
            set { if (_drawAdapter != null) { _drawAdapter.HasGrid = value; } }
        }

        public PixelRenderer(int plateWidth, int plateHeight, byte pixelSize)
        {
            _drawAdapter = new PixelDrawAdapter();
            _plate = new PixelPlate(plateWidth, plateHeight); 
            _algo = new PixelAlgo();
            _stateMachineFirst = new SimpleStateMachine();
            _stateMachineSecond = new SimpleStateMachine();

            _drawAdapter.Plate = _plate;
            _drawAdapter.PixelSize = pixelSize;
            _drawAdapter.HasGrid = true;

            _algo.Plate = _plate;

            //BEGIN ---- FIRST ACTION -----------------------------------------
            /*--1--*/
            _stateMachineFirst.PushAction((Point? point) =>
                {
                    if (point.HasValue)
                    {
                        DrawPlotPixel(point.Value, Helper.GetRandomColor().ToPixelColor());
                    }
                });
            /*--2--*/
            _stateMachineFirst.PushAction((Point? point) =>
            {
                Point? p = _drawAdapter.ToPlateCoords(point.Value);

                if (p.HasValue)
                {
                    DrawPlotPixel(point.Value, color: Color.Black.ToPixelColor());
                    _plate.MoveTo(p.Value.X, p.Value.Y);
                }
            });
            /*--3--*/
            _stateMachineFirst.PushAction((Point? point) =>
            {
                Point? p = _drawAdapter.ToPlateCoords(point.Value);

                if (p.HasValue)
                {
                    _plate.SlowFill(p.Value.X, p.Value.Y, Color.Red.ToPixelColor());
                    Draw();
                }
            });
            /*--4--*/
            _stateMachineFirst.PushAction((Point? point) =>
            {
                Point? p = _drawAdapter.ToPlateCoords(point.Value);

                if (p.HasValue)
                {
                    _algo.AddPoint(p.Value);
                    Draw();
                }
            });
            //END ---- FIRST ACTION --------------------------------------------

            //BEGIN ---- SECOND ACTION -----------------------------------------
            /*--1--*/
            _stateMachineSecond.PushAction((Point? point) =>
            {
                if (point.HasValue)
                {
                    DrawCleanPixel(point.Value);
                    Draw();
                }
            });
            /*--2--*/
            _stateMachineSecond.PushAction((Point? point) =>
            {
                Point? p = _drawAdapter.ToPlateCoords(point.Value);

                if (p.HasValue)
                {
                    _plate.LineTo(p.Value.X, p.Value.Y, Color.Black.ToPixelColor());
                    _plate.MoveTo(p.Value.X, p.Value.Y);
                    Draw();
                }
            });
            /*--3--*/
            _stateMachineSecond.PushAction((Point? point) =>
            {
                Point? p = _drawAdapter.ToPlateCoords(point.Value);
                if (p.HasValue)
                {
                    var xProj = Math.Abs(p.Value.X - _plate.BeginX);
                    var yProj = Math.Abs(p.Value.Y - _plate.BeginY);
                    var rad = (int)Math.Sqrt((double)(xProj * xProj + yProj * yProj));
                    _plate.AddCircle(rad, Color.Black.ToPixelColor());
                    Draw();
                }
            });
            /*--4--*/
            _stateMachineSecond.PushAction((Point? point) =>
            {
                Point? p = _drawAdapter.ToPlateCoords(point.Value);
                if (p.HasValue)
                {
                    _algo.CloseFigure();
                    Draw();
                }
            });
            //END ---- SECOND ACTION -----------------------------------------

            this.MouseMove += PixelRenderer_MouseMove;
            this.MouseDown += PixelRenderer_MouseDown;
            this.KeyDown += PixelRenderer_KeyDown;
        }

        void PixelRenderer_KeyDown(object sender, KeyEventArgs e)
        {
            Color stateColor;
            switch (e.KeyCode)
            {
                case Keys.D1: _stateMachineFirst.CurrentState = 0;
                    _stateMachineSecond.CurrentState = 0;
                    stateColor = Color.Red;
                    _allowedMouseMoveAction = true;
                    break;
                case Keys.D2: _stateMachineFirst.CurrentState = 1;
                    _stateMachineSecond.CurrentState = 1;
                    stateColor = Color.Green;
                    _allowedMouseMoveAction = false;
                    break;
                case Keys.D3: _stateMachineFirst.CurrentState = 1;
                    _stateMachineSecond.CurrentState = 2;
                    stateColor = Color.Blue;
                    _allowedMouseMoveAction = false;
                    break;
                case Keys.D4: _stateMachineFirst.CurrentState = 2;
                    _stateMachineSecond.CurrentState = SimpleStateMachine.UNDEFINED_STATE;
                    stateColor = Color.Aqua;
                    _allowedMouseMoveAction = false;
                    break;
                case Keys.D5: _stateMachineFirst.CurrentState = 3;
                    _stateMachineSecond.CurrentState = 3;
                    stateColor = Color.Purple;
                    _allowedMouseMoveAction = false;
                    break;
                default: _stateMachineFirst.CurrentState =
                    _stateMachineSecond.CurrentState = SimpleStateMachine.UNDEFINED_STATE;
                    stateColor = Color.Black;
                    _allowedMouseMoveAction = false;
                    break;
            }
            _plate[0, 0].IsFull = true;
            _plate[0, 0].MainColor = stateColor.ToPixelColor();
            Draw();
        }

        void PixelRenderer_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                _stateMachineFirst.ResolveCurrentState(e.Location);
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                _stateMachineSecond.ResolveCurrentState(e.Location);
            }
        }
        
        void PixelRenderer_MouseMove(object sender, MouseEventArgs e)
        {
            if (_allowedMouseMoveAction)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    _stateMachineFirst.ResolveCurrentState(e.Location);
                }
                else if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    _stateMachineSecond.ResolveCurrentState(e.Location);
                }
            }
        }

        public bool DrawPlotPixel(Point p, PixelColor? color = null)
        {
            var pp = _drawAdapter.ToPlateCoords(p);
            bool outValue = _plate.Plot(pp, color: color);
            if (outValue) { Draw(); }
            return outValue;
        }
        public bool DrawCleanPixel(Point p)
        {
            bool outValue = _plate.Clean(_drawAdapter.ToPlateCoords(p));
            if (outValue) { Draw(); }
            return outValue;
        }

        public void ResizeContext()
        {
            if (Width < 1 || Height < 1) { return; }

            if (BackgroundImage != null)
            {
                BackgroundImage.Dispose();
            }
            BackgroundImage = new Bitmap(Width, Height);

            _drawAdapter.Context = BackgroundImage as Bitmap;
            _drawAdapter.ApplyChanges();
        }
        public void Draw()
        {
            _drawAdapter.Draw();
            Invalidate();
        }
    }
}
